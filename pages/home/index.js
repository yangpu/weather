// 引用百度地图微信小程序JSAPI模块 
const bmap = require('../../libs/bmap-wx.js'); 
//获取应用实例
const app = getApp()
Page({ 
    data: { 
        weatherData: '' ,
        BMap: null
    }, 
    onLoad () { 
        this.init()
    },

    init () {
        let BMap = new bmap.BMapWX({ 
            ak: app.globalData.ak 
        });

        BMap.weather({ 
            fail: this.initBmapFail, 
            success: this.initBmapSuccess 
        }); 

        this.setData({
            BMap
        })
    },

    // 初始化bmap成功回调
    initBmapSuccess (data) {
        console.log(this)
        console.log(data)
        var weatherData = data.currentWeather[0]; 
        weatherData = '城市：' + weatherData.currentCity + '\n' + 'PM2.5：' + weatherData.pm25 + '\n' +'日期：' + weatherData.date + '\n' + '温度：' + weatherData.temperature + '\n' +'天气：' + weatherData.weatherDesc + '\n' +'风力：' + weatherData.wind + '\n'; 
        this.setData({ 
            weatherData: weatherData 
        }); 
    },

    initBmapFail (data) {
        console.log(this)
        console.log(data)
    }
})